# poc-tracker
A web application that allows user to submit addresses of 1 pickup point and 1 drop-off point. It then displays the waypoints returned from the backend on the map.
​
## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Lints and fixes files
```
npm run lint
```

### Google Maps API
To run the google maps in this application you would need to feed your personal Google Maps API key on the top of the file: 
```
./src/utils/gmaps.js
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
