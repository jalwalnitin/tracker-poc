import { expect } from "chai";
import sinon from "sinon";
import App from "@/App.vue";

describe("App.vue", () => {
  it("hide popup", () => {
    //const wrapper = shallowMount(App, {});
    App.methods.hidePopUp();
    expect(App.data().popUpVisible).to.equal(false);
  });

  describe("#sendRoutingRequest", () => {
    let showPopUpStub;
    let makeFetchRequestWithPlacesStub;
    let getRouteStub;

    beforeEach(function () {
      showPopUpStub = sinon.stub(App.methods, "showPopUp");
      getRouteStub = sinon.stub(App.methods, "getRoute");
      makeFetchRequestWithPlacesStub = sinon.stub(
        App.methods,
        "makeFetchRequestWithPlaces"
      );
    });

    afterEach(function () {
      makeFetchRequestWithPlacesStub.restore();
      showPopUpStub.restore();
      getRouteStub.restore();
    });

    it("should show popup on server error", async () => {
      makeFetchRequestWithPlacesStub.returns(Promise.reject("server error!!!"));
      await App.methods.sendRoutingRequest(
        '{"origin":"Innocentre, Hong Kong","destination":"Hong Kong International Airport Terminal 1"}'
      );

      expect(
        showPopUpStub.calledWithExactly({
          heading: "server error!!!",
          message: "Please try again later.",
          buttonText: "OK",
        })
      ).to.equal(true);
    });

    it("should try to get token on receiving origin and destination from user", async () => {
      makeFetchRequestWithPlacesStub.returns(Promise.resolve("token"));
      await App.methods.sendRoutingRequest(
        '{"origin":"Innocentre, Hong Kong","destination":"Hong Kong International Airport Terminal 1"}'
      );

      expect(getRouteStub.calledWithExactly("token")).to.equal(true);
    });
  });

  describe("#getRoute", () => {
    let showPopUpStub;
    let makeFetchRequestWithTokenStub;
    let handleRouteResponse;

    beforeEach(function () {
      showPopUpStub = sinon.stub(App.methods, "showPopUp");
      handleRouteResponse = sinon.stub(App.methods, "handleRouteResponse");
      makeFetchRequestWithTokenStub = sinon.stub(
        App.methods,
        "makeFetchRequestWithToken"
      );
    });

    afterEach(function () {
      makeFetchRequestWithTokenStub.restore();
      showPopUpStub.restore();
      handleRouteResponse.restore();
    });

    it("should show popup on server error", async () => {
      makeFetchRequestWithTokenStub.returns(Promise.reject("server error!!!"));
      await App.methods.getRoute(
        '{"origin":"Innocentre, Hong Kong","destination":"Hong Kong International Airport Terminal 1"}'
      );

      expect(
        showPopUpStub.calledWithExactly({
          heading: "server error!!!",
          message: "Please try again later.",
          buttonText: "OK",
        })
      ).to.equal(true);
    });

    it("should try to get route on receiving token and handle the route response", async () => {
      makeFetchRequestWithTokenStub.returns(Promise.resolve("route"));
      await App.methods.getRoute("token");

      expect(handleRouteResponse.calledWithExactly("route")).to.equal(true);
    });
  });

  describe("#handleRouteResponse", () => {
    let showPopUpStub;
    let traceWaypointsStub;
    let updateFormDetailsStub;
    let routeResponse;
    let getRouteStub;

    beforeEach(function () {
      getRouteStub = sinon.stub(App.methods, "getRoute");
      showPopUpStub = sinon.stub(App.methods, "showPopUp");
      traceWaypointsStub = sinon.stub(App.methods, "traceWaypoints");
      updateFormDetailsStub = sinon.stub(App.methods, "updateFormDetails");
    });

    afterEach(function () {
      getRouteStub.restore();
      traceWaypointsStub.restore();
      showPopUpStub.restore();
      updateFormDetailsStub.restore();
    });

    context("If route status is success", function () {
      beforeEach(function () {
        routeResponse = {
          status: "success",
          total_distance: "500",
          total_time: "100",
        };
      });

      it("should trace waypoints", () => {
        App.methods.handleRouteResponse(routeResponse);

        expect(traceWaypointsStub.calledWithExactly(routeResponse)).to.equal(
          true
        );
      });

      it("should update the distance and time details in form", () => {
        App.methods.handleRouteResponse(routeResponse);

        expect(updateFormDetailsStub.calledWithExactly(routeResponse)).to.equal(
          true
        );
      });
    });

    context("If route status is failure", function () {
      beforeEach(function () {
        routeResponse = {
          status: "failure",
          error: "Location not accessible by car",
        };
      });

      it("should pass info 'Location not accessible by car' to form", () => {
        App.methods.handleRouteResponse(routeResponse);

        expect(updateFormDetailsStub.calledWithExactly(routeResponse)).to.equal(
          true
        );
      });
    });

    context("If route status is in progress", function () {
      beforeEach(function () {
        routeResponse = {
          status: "in progress",
        };
      });

      it("should show popup that server is busy and will retry in 3 seconds", () => {
        App.methods.handleRouteResponse(routeResponse);

        expect(
          showPopUpStub.calledWithExactly({
            heading: "Server busy!",
            message: "Retrying in 3 seconds.",
            buttonText: "",
          })
        ).to.equal(true);
      });

      it("should request to get route again in 3 seconds", async () => {
        var clock = sinon.useFakeTimers({ now: Date.now() });
        App.methods.handleRouteResponse(routeResponse);

        clock.tick(3100);
        expect(getRouteStub.called).to.equal(true);
      });
    });
  });
});
