import { expect } from "chai";
import { shallowMount } from "@vue/test-utils";
import PopUp from "@/components/Popup.vue";

describe("Popup.vue", () => {
  it("renders props.header to the Heading when passed", () => {
    const header = "Heading";
    const wrapper = shallowMount(PopUp, {
      propsData: { header },
    });
    expect(wrapper.find("h1").text()).to.equal(header);
  });
});
